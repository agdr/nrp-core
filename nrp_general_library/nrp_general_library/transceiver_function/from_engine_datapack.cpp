//
// NRP Core - Backend infrastructure to synchronize simulations
//
// Copyright 2020-2021 NRP Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This project has received funding from the European Union’s Horizon 2020
// Framework Programme for Research and Innovation under the Specific Grant
// Agreement No. 945539 (Human Brain Project SGA3).
//

#include "nrp_general_library/transceiver_function/from_engine_datapack.h"

#include "nrp_general_library/transceiver_function/transceiver_function_interpreter.h"
#include "nrp_general_library/utils/nrp_exceptions.h"

EngineDataPack::EngineDataPack(const std::string &keyword, const DataPackIdentifier &datapackID, bool isPreprocessed)
    : _keyword(keyword),
      _datapackID(datapackID),
      _isPreprocessed(isPreprocessed)
{}

EngineClientInterface::datapack_identifiers_set_t EngineDataPack::getRequestedDataPackIDs() const
{
    // If this is a preprocessing function this and _datapackID must be linked to the same engine
    if(this->isPrepocessing() && this->_datapackID.EngineName != this->linkedEngineName())
        throw NRPException::logCreate("Preprocessing function is linked to engine \"" + this->linkedEngineName() +
        "\" but its input datapack \""+ this->_datapackID.Name + "\" is linked to engine \"" + this->_datapackID.EngineName +
        "\". Preprocessing functions can just take input datapacks from their linked engines");

    return _isPreprocessed ? EngineClientInterface::datapack_identifiers_set_t() : EngineClientInterface::datapack_identifiers_set_t({this->_datapackID});
}

boost::python::object EngineDataPack::runTf(boost::python::tuple &args, boost::python::dict &kwargs)
{
    const auto engineDevs = TransceiverDataPackInterface::TFInterpreter->getEngineDataPacks();

    bool foundDevID = false;
    auto engDataPacksIt = engineDevs.find(this->_datapackID.EngineName);
    if(engDataPacksIt != engineDevs.end())
    {
        for(const auto &curDataPack : *(engDataPacksIt->second))
        {
            if(curDataPack->id().Name == this->_datapackID.Name)
            {
                kwargs[this->_keyword] = curDataPack;

                foundDevID = true;
                break;
            }
        }
    }

    if(!foundDevID)
        throw NRPException::logCreate("Couldn't find datapack with ID name \"" + this->_datapackID.Name + "\"");

    return TransceiverDataPackInterface::runTf(args, kwargs);
}
