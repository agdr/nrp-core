//
// NRP Core - Backend infrastructure to synchronize simulations
//
// Copyright 2020-2021 NRP Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This project has received funding from the European Union’s Horizon 2020
// Framework Programme for Research and Innovation under the Specific Grant
// Agreement No. 945539 (Human Brain Project SGA3).
//

#include "nrp_general_library/transceiver_function/transceiver_function_manager.h"
#include "nrp_general_library/transceiver_function/transceiver_function_interpreter.h"

#include "nrp_general_library/utils/nrp_exceptions.h"

#include <iostream>

EngineClientInterface::datapack_identifiers_set_t TransceiverFunctionManager::updateRequestedDataPackIDs() const
{
    return this->_tfInterpreter.updateRequestedDataPackIDs();
}

void TransceiverFunctionManager::loadTF(const nlohmann::json &tfConfig)
{
    auto storedConfigIterator = this->_tfSettings.find(tfConfig);
    std::string tf_name = tfConfig.at("Name");
    auto loadedTF = this->_tfInterpreter.findTransceiverFunction(tf_name);
    if(loadedTF != this->_tfInterpreter.getLoadedTransceiverFunctions().end() || storedConfigIterator != this->_tfSettings.end())
        throw NRPException::logCreate("TF with name " + tf_name + " already loaded");

    this->_tfSettings.insert(tfConfig);
    this->_tfInterpreter.loadTransceiverFunction(tfConfig);
}

void TransceiverFunctionManager::updateTF(const nlohmann::json &tfConfig)
{
    this->_tfInterpreter.updateTransceiverFunction(tfConfig);
    this->_tfSettings.insert(tfConfig);
}

TransceiverFunctionManager::tf_results_t TransceiverFunctionManager::executeActiveLinkedTFsGeneric(const std::string &engineName, const bool preprocessing)
{
    tf_results_t tfResults;

    const auto linkedTFRange = this->_tfInterpreter.getLinkedTransceiverFunctions(engineName);

    for(auto curTFIt = linkedTFRange.first; curTFIt != linkedTFRange.second; ++curTFIt)
    {
        if(this->isActive(curTFIt->second.Name) && (curTFIt->second.TransceiverFunction->isPrepocessing() == preprocessing))
        {
            // Get datapack outputs from transceiver function
            TransceiverFunctionInterpreter::datapack_list_t pyResult(this->_tfInterpreter.runSingleTransceiverFunction(curTFIt->second.Name));
            TransceiverFunctionInterpreter::TFExecutionResult result(std::move(pyResult));

            // Extract pointers to retrieved datapacks
            result.extractDataPacks();
            tfResults.push_back(result);
        }
    }

    return tfResults;
}

TransceiverFunctionManager::tf_results_t TransceiverFunctionManager::executeActiveLinkedPFs(const std::string &engineName)
{
    return executeActiveLinkedTFsGeneric(engineName, true);
}

TransceiverFunctionManager::tf_results_t TransceiverFunctionManager::executeActiveLinkedTFs(const std::string &engineName)
{
    return executeActiveLinkedTFsGeneric(engineName, false);
}

bool TransceiverFunctionManager::isActive(const std::string &tfName)
{
    for(const auto &curSetting : this->_tfSettings)
    {
        if(curSetting.at("Name") == tfName)
            return curSetting.at("IsActive");
    }

    return false;
}

TransceiverFunctionInterpreter &TransceiverFunctionManager::getInterpreter()
{   return this->_tfInterpreter;    }
